 [**BACK**](javascript:history.go(-1))

### TELANGANA LABOUR WELFARE FUND ACT, 1987

ACT NO. 34 Of 1987  
STATEMENT OF OBJECTS AND REASONS

With the rapid growth of industries in the State welfare of the labour has assumed great importance. It has been felt that welfare measures of the labour are best administered by a non official agency as is being done in the States of Maharashtra and Gujarat where statutory Welfare Board s have been constituted and entrusted with the duty of looking after the welfare of the labour. It was agreed in the Labour Minister s Conference held at Bangalore in the month of October, 1961 that all States should enact a law for the constitution of statutory Labour Welfare Fund on the analogy of the law existing in the States of Maharashtra and Gujarat; and the National Commission for Labour also considered the need for such statutory Labour Welfare Boards in all the States. The Andhra Pradesh State Labour Advisory Board had recommended for a similar legislation for this State also, on the lines of the Maharashtra enactment.

After taking into consideration the recommendations made by the Labour Minister s Conference, the National Commission of Labour and the Andhra Pradesh State Labour Advisory Board, the Government have decided to undertake legislation for the establishment of a Labour welfare fund in this State and for the constitution of a Labour Welfare Board for this State for the purpose of administering the fund and to carry on such other functions as are assigned to the said Board, from time to time

This Bill seeks to give effect to the above decision.

### TELANGANA LABOUR WELFARE FUND ACT, 1987

Act No 34 of 1987  
ARRANGEMENT OF SECTIONS

[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s1)2.  [Short title and commencement](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s1)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s2)
3.  [Definitions.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s2)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s3)
4.  [Constitution of Andhra Pradesh Labour Welfare Fund.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s3)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s4)
5.  [Establishment and functions of Andhra Pradesh Labour Welfare Board.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s4)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s5)
6.  [Disqualifications and removal.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s5)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s6)
7.  [Resignation of office by member and filling up casual vacancies.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s6)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s7)
8.  [Power to appoint Committees.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s7)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s8)
9.  [Unpaid accumulations transferred to the Fund and claims thereto.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s8)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s9)
10.  [Interest on unpaid accumulation of fines after notice of demand.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s9)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s10)
11.  [Contribution to the Fund by employee and employer.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s10)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s11)
12.  [Grants and advances by the Government.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s11)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s12)
13.  [Vesting and application of the Fund.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s12)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s13)
14.  [Power of Board to borrow.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s13)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s14)
15.  [Deposits of Fund and placing of accounts and audit report before the Government.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s14)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s15)
16.  [Investment of Fund.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s15)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s16)
17.  [Directions by Government to Board.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s16)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s17)
18.  [Appointment and powers of Welfare Commissioner.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s17)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s18)
19.  [Appointment of Inspectors.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s18)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s19)
20.  [Appointment of clerical and other staff by Board.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s19)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s20)
21.  [Power of Government over staff of Board in certain cases.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s20)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s21)
22.  [Allotment of certain officers and staff to the Board.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s21)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s22)
23.  [Transfer of Provident Fund to the Board.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s22)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s23)
24.  [Powers of Government or authorised officer to call for records, etc.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s23)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s24)
25.  [Mode of recovery of sums payable into the Fund, etc.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s24)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s25)
26.  [Penalty for obstructing inspection or for failure to produce documents etc.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s25)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s26)
27.  [Cognizance of offences.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s26)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s27)
28.  [Offences by companies.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s27)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s28)
29.  [Limitation of prosecution.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s28)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s29)
30.  [Power to supervise the welfare activities of an establishment.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s29)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s30)
31.  [Penalty for non compliance with the direction of the Board.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s30)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s31)
32.  [Annual Report.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s31)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s32)
33.  [Supersession of the Board.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s32)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s33)
34.  [Delegation of powers.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s33)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s34)
35.  [Members of Board, Welfare Commissioner, Inspectors and all officers and servants of the Board to be public servants.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s34)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s35)
36.  [Protection of action taken in good faith.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s35)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s36)
37.  [Exemption.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s36)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s37)
38.  [Powers of Government to make rules.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s37)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s38)
39.  [Power of Board to make regulations.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s38)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s39)
40.  [Amendment of Section 8 of the Central Act 4 of 1936.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s39)[](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s40)
41.  [Amendment of Section 8 of Central Act 8 of 1923.](file://rus100/data/labour/ActsOfLabourDept/AP-Laws/lwfa.html#s40)

Reserved by the Governor on the 2nd September, 1986 for the consideration and assent of the President. Received the assent of the President on the 14th August, 1987 and first published on the 19th August, 1987 in the Andhra Pradesh Gazette Part IV B (Ext.)

An Act to provide for the Constitution of a Welfare Fund for the financing of activities to promote welfare of labour in the State of Andhra Pradesh and for the establishment of Board for conducting such activities and for matters connected therewith.

Be it enacted by the Legislative Assembly of the State of Andhra Pradesh in the Thirty eight Year of the Republic of India as follows:

**1\. Short title and commencement:-** (1) This Act may be called the Andhra Pradesh Labour Welfare Fund Act, 1987.

(2) It shall come into force on such date as the State Government may, by notification, appoint.

**2.** In this Act, unless the context otherwise requires,

(1) Board means the Andhra Pradesh Labour Welfare Board established under Section 4;

(2) employee means:

(i) any person who is employed for hire or reward to do any work, skilled or unskilled, manual, supervisory, clerical, or technical, in an establishment for a period of thirty days during the period of twelve months, whether the terms of employment be express or implied; but does not include any person

(a) who is employed mainly in a managerial capacity; or

(b) who being employed in a supervisory capacity, draws wages exceeding Rs. 1,600 (Rupees sixteen hundred) per mensum or exercises either by the nature of the duties attached to the office or by reason of the powers vested in him, functions mainly of a managerial nature; or

(c) who is employed as an apprentice or on part time basis.

_Explanation:-_ An apprentice means a person who according to the Certified Standing Orders applicable to the establishment is an apprentice or who is declared to be an apprentice by the authority specified in this behalf by the Government; and

(ii) any other person employed in any establishments whom the Government, may by notification, declare to be an employee for the purposes of this Act;

(3) employer means any person who employs either directly or through another person either on behalf of himself or any other person, one or more employees in an establishment and includes

(i) in a factory, any person named under Section 7(1) (f) of the Factories Act, 1948 as the manager;

(ii) in any establishment, any person responsible to the owner for the supervision and control of the employees or for the payment of wages;

(4) establishment means

(i) a factory as defined in Section 2(m) of the Factories Act, 1948;

(ii) a motor transport undertaking as defined in the Motor Transport Workers Act, 1961;

(iii) any other establishment as defined in Section 2 (10) of the Andhra Pradesh Shops and Establishments Act, 1966 and includes a society registered under any law in force in the State relating to registration of societies, and a chartiable or other trust, whether registered or not, which carries on any business or trade or any work in connection with or ancillary thereto and which employs or on any working day during the preceding twelve months employed twenty or more persons, but does not include an establishment, not being a factory, belonging to or under the control of the Central or any State Government;

(5) Fund means the Andhra Pradesh Labour Welfare Fund constituted under Section 3;

(6) Government means the State Government;

(7) independent member means a member of the Board who is not connected with the management of any establishment or who is not employee; and includes an officer of the Government nominated as a member;

(8) Inspector means an Inspector appointed under Section 18;

(9) notification means a notification published in the Andhra Pradesh Gazette and the word notified shall be construed accordingly;

(10) prescribed means prescribed by the Government by the rules made under this Act;

(11) unpaid accumulations means all payments due to an employee but not paid to him within a period of three years from the date on which they became due, whether before or after the commencement of this Act, including the wages and gratuity legally payable, but does not include the amount of contribution, if any paid by an employer to a Provident Fund established under the Employees Provident Funds Act, 1952;

(12) Wages means all remuneration capable of being expressed in terms of money which would, if the terms of the contract of employment, express or implied were fulfilled, be payable to an employee in respect of his employment or of work done in such employment and includes bonus payable under the Payment of Bonus Act, 1965, but does not include,

(a) the value of any house accommodation, supply of light, water, medical attendance, or any other amenity or any service excluded from the computation of wages by general or special order of the Government.

(b) any contribution paid by the employer to any pension fund or provident fund or under any scheme of social insurance;

(c) any travelling allowance or the value of any travelling concession;

(d) any sum paid to the employee to defray special expenses entailed on him by the nature of his employment; or

(e) any gratuity payable on termination of employment; (13) Welfare Commissioner means the Welfare Commissionce appointed under Section 17.

**3\. Constitution of Andhra Pradesh Labour Welfare Fund:-** (1) The Government shall constitute a fund to be called the Andhra Pradesh Labour Welfare Fund, and notwithstanding anything contained in any other law for the time being in force or in any contract or instrument, all unpaid accumulations shall be paid, at such intervals as may be prescribed, to the Board, and be credited to the Fund and the Board shall keep a separate account therefor until claims thereto have been decided in the manner, provided for in Section 8.

(2) There shall also be credited to the Fund:

(a) unpaid accumulations paid to the Board under Section 8;

(b) all fines including the amount realised under Standing Orders issued under the Andhra Pradesh Industrial Employment (Standing Orders) Rules, 1953 from the employees by the employees notwithstanding anything contained in any agreement between the employer and the employee.

(c) deductions made under the proviso to sub section (2) of Section 9 of the Payment of Wages Act, 1936.

(d) contribution by employers and employees;

(e) any interest by way of penalty paid under Section 9;

(f) any voluntary donations;

(g) any amount raised by the Board from other sources to augment the resources of the Board;

(h) any fund transferred under sub section (5) of Section 12;

(i) any sum borrowed under Section 13;

(j) grants or advances made by the Government.

(k) any money deposited under sub section (1) of Section 8 of the Workmen s Compensation Act, 1923 as compensation in respect of a deceased workman where the Commissioner for Workmen s Compensation is satisfied after necessary enquiry that no dependent exists, subject however, to the deductions permissible under the said sub section; as also any amount remaining undisbursed out of such deposits.

(3) The sums specified in sub section (2) shall be paid to, or collected by, such agencies, at such intervals and in such manner, and the accounts of the Fund shall be maintained and audited in such manner as may be prescribed.

**4\. Establishment and functions of Andhra Pradesh Labour Welfare Board:-** (1) The Government shall, by notification establish a Board for the whole of the State of Andhra Pradesh to be called the "Andhra Pradesh Labour Welfare Board" for the purpose of administering the Fund and to carry on such other functions as are assigned to the Board by or under this Act.

(2) The Board shall be a body corporate by the name of the Andhra Pradesh Labour Welfare Board having perpetual succession and a common seal, with power to acquire, hold and dispose off property, both movable and immovable, and shall, by the said name, sue and be sued.

(3) The Board shall consist of such number of members as may be prescribed in each of the following categories, namely:

(a) representatives of employers and employees in equal proportion, to be nominated by the Government;

(b) independent members, to be nominated by the Government, as follows:

(i) Minister for Labour who shall be the Chairman;

(ii) the Secretary to Government, Labour Employment, Nutrition and Technical Education Department, ex officio;

(iii) the Secretary to Government, Finance and Planning (Finance) Department, ex officio;

(iv) the Secretary to Government, Industries Department, ex officio;

(v) a woman member to represent women

(4) Save as otherwise expressly provided in this Act, the term of office of the members of the Board shall be three years commencing on the date on which their names are notified.

(5) The allowances, if any, payable to the members of the Board and the conditions of nomination of the representatives of the employers and employees shall be such as may be prescribed.

**5\. Disqualifications and removal:-** (1) No person shall be nominated as or continue to be, a member of the Board who

(a) is a salaried official of the Board; or

(b) is or at any time has been adjudged insolvent or has suspended payment of his debts or has compounded with his creditors; or

(c) is found to be of unsound mind, or;

(d) is or has been convicted by a Criminal Court of an offence involving moral turpitude, unless such conviction has been set aside; or

(e) is in arrears of any sum due to the Board.

(2) The Government may remove from office any member who

(a) is or has become subject to any of the disqualifications mentioned in sub section (1) or

(b) is absent without leave of the Board for more than three consecutive meetings of the Board;

Provided that before taking action under this sub section, the member concerned shall be given an opportunity to make his representation against the action proposed.

**6\. Resignation of office by member and filling up casual vacancies:-** (1) A member may resign his office by giving notice thereof in writing to the Government, and on the resignation being accepted, he shall be deemed to have vacated his office as such.

(2) A casual vacancy in the office of a member shall be filled by nomination of another person from the concerned category and a member so nominated shall hold office for the unexpired portion of the term of the office of his predecessor.

(3) No act or proceedings of the Board shall be invalid on the ground merely of the existence of any vacancy in, or any defect in the constitution of, the Board.

**7\. Power to appoint Committees:-** For the purpose of advising the Board in the discharge of its functions and in particular for carrying into effect any of the matters specified in sub section (2) of Section 12, the Board may constitute one or more committees consisting of at least one member of the Board and equal number of representatives of employees and employers.

**8\. Unpaid accumulations transferred to the Fund and claims thereto:-** (1) All unpaid accumulations shall, subject to other provisions of this section, be deemed to be abandoned property and be transferred to the control and administration of the Board and shall vest in the Board.

(2) Any unpaid accumulations transferred to the Board in accordance with the provisions of this Act shall, on such transfer, discharge an employer of the liability to make payment to an employee in respect thereof, but to the extent only of the amount transferred to the Board; and the liability to make payment to the employee to the extent aforesaid shall be deemed to be transferred to the Board.

(3) As soon as possible, after the transfer of any unpaid accumulations to the Board, the Board shall publish a notice, containing such particulars as may be prescribed, in the following manner:

(a) by exhibiting on the notice board of the establishment in which the unpaid accumulations were earned;

(b) by publishing in the Andhra Pradesh Gazette;and

(c) by publishing in any two newspapers having wide circulation, and in the language commonly understood, in the area in which the establishment in which the unpaid accumulations were earned, is situate, or in such other manner as may be prescribed, regard being had to the amount of the claim; and shall invite claims by employees for any payment due to them. The notice shall be so published ordinarily in the months of June and December or every year, for a period of three years, from the date of the transfer to the unpaid accumulations to the Board.

(4) Where any question arises whether the notice referred to in sub section (3) was given publicity as referred to in sub section (3) was given publicity as effect given by the Board shall be conclusive proof of such publication.

(5) Where a claim is received whether in pursuance of the notice aforesaid or otherwise, within a period of four years from the date of first publication of the notice in respect of such claim, the Board shall transfer such claim to the authority appointed under Section 15 of the Payment of Wages Act. 1936, having jurisdiction in the area in which the establishment is situate, and the said authority shall proceed to adjudicate upon, and decide such claim; and in hearing such claim, the said authority shall have the same powers conferred by and follow the same procedure, in so far as it is applicable as laid down by or under the provisions of that Act.

(6) If the said authority, after making the enquiry into the validity of any claim, is satisfied that any such claim is valid and the employee is entitled to receive payment, it shall pass an order directing that the unpaid accumulation or any part thereof in relation to which the claim is made shall cease to be deemed to be abandoned property and to be transferred to the Board and that the Board shall pay the whole or such part of the accumulation as the authority decides to be property due, to the employee; and the Board shall make the payment accordingly:

Provided that the Board shall not be liable pay any sum in excess of that transferred under sub section (1) of Section 3 to the Board as unpaid accumulations in respect of the claim.

(7) Where a claim for payment is refused by the said authority, the employee shall have a right of appeal in the cities of Hyderabad and Secunderabad, to the Court of Small Causes, and elsewhere, to the District Court, and the Board shall comply with any order made by the Court in such appeal. Every appeal under this sub section shall lie within sixty days from the date of communication of the order of refusal of the said authority.

(8) The decision of the said authority if no appeal is filed, and the decision of the Court in appeal, shall be final and conclusive as to the right to receive payment, as to the liability of the Board to pay and also as to the amount, if any, so payable.

(9) Where no claim is made within the time specified in sub section (5) or a claim made has been refused as aforesaid by the said authority and no appeal has been filed within the time allowed for such appeal or an appeal filed has been dismissed by the court, as the case may be, then the unpaid accumulations in respect of such claim shall accrue to, and vest in, the State as bona vacantia , and shall thereafter, without further enquiry and declaration, be deemed to be transferred to, and form part of the Fund.

**9\. Insert on unpaid accumulation of fines after notice of demand:-** (1) If an employer does not pay to the Board any amount of unpaid accumulations of fines realised from the employees, within the time specified therefor by or order this Act, the Welfare Commissioner may serve or cause to be served a notice on such employer to pay the amount within the period specified therein which shall not less than thirty days from the date of service of such notice.

(2) If the employer fails, without sufficient cause, to pay any such amount within the period specified in the notice, he shall, in addition to the amount, pay by way of penalty to the Board simple interest:

(a) for the first three months at one per cent of the said amount for each complete month or part thereof after the last date by which he should have paid it according to the notice; and

(b) for each complete month or part thereafter at one and half percent of that amount during the time he continues to make default in the payment of that amount.

Provided that the Welfare Commissioner may, subject to such conditions as may be prescribed, remit the whole or any part of the penalty in respect of any period.

**10\. Contribution to the Fund by employee and employer:-** (1) Every employee shall contribute two rupees per year to the Fund and every employer shall, in respect of each such employee, contribute five rupees per year to the Fund.

(2) Notwithstanding anything contained in any other law for the time being in force, the employer shall be entitled to recover from the employees the employees contribution by deduction from is his wages in such manner as may be prescribed and such deduction shall be deemed to be a deduction authorised by or under the Payment of Wages Act, 1936.

**11\. Grants and advances by the Government:-** The Government may, from time to time, make grants and advance loans to the Board to the extent of the Fund available for the purposes of this Act, on such terms and conditions as the Government may, in each case, determine.

**12\. Vesting and application of the Fund:-** (1) The Fund shall vest in and be held and applied by the Board as trustees subject to the provisions and for the purposes of this Act. The moneys therein shall be utilized by the Board to defray the cost of carrying out measures which may be specified by the Government, from time to time, for promoting the welfare of labour and of their dependents.

(2) Without prejudice to the generality of the provisions in sub section (1), the money in the Fund may be utilized by the Board to defray expenditure on the following:

(a) labour welfare centres under the control of the Labour Department of the Government;

(b) reading rooms and libraries;

(c) games and sports;

(d) community necessities;

(e) excursions tours and holiday homes;

(f) entertainment and other forms of recreation;

(g) home industries and subsidiary occupations for women and unemployed persons;

(h) corporate activities of social nature;

(i) vocational training;

(j) convalescent homes for tuberculosis patients;

(k) pre schools;

(l) nutritious food to children of employees;

(m) construction and maintenance of the labour welfare centre buildings;

(n) cost of administering the Act including the salaries and allowances of the staff appointed for the purposes of the Act;

(o) medical aid to employees for specialised treatment in deserving cases; and

(p) such other objects as would in the opinion of the Government improve the standard of living and ameliorate the social conditions of labour;

Provided that the Fund shall not be utilized in financing any measure which the employer is required under any law for the time being in force to carry out;

Provided further that unpaid accumulations and fines shall be paid to the Board and be expended by it under this Act, notwithstanding anything in the Payment of wages Act, 1936, or any other law for the time being in force..

(3) The Board may, with the approval of the Government make a grant out of the Fund to any employer, any local authority or any other body in aid of any activity for the welfare or labour approved by the Government.

(4) If any question arises whether any particular expenditure is or is not debitable to the Fund, the matter shall be referred to the Government, whose decision thereon shall be final.

(5) It shall be lawful for the Board to continue any activity financed from the labour welfare fund of any establishment, if the said fund is duty transferred to the Board.

**13\. Power of Board to borrow :-** Subject to the other provisions of this Act the Board may, from time to time, with the previous sanction of the Government, and subject to such conditions as may be specified by the Government in this behalf borrow any sum required for the purposes of the Act.

**14\. Deposits of Fund and placing of accounts and audit report before the Government:-** (1) All moneys and receipts forming part of the Fund shall be deposited in the Reserve Bank of India constituted under the Reserve Bank of India Act, 1934, or in the State Bank of India, constituted under the State Bank of India Act, 1955, or any corresponding new Bank as defined in the Banking Companies (Acquisition and Transfer of Undertakings) Act, 1970, or any other schedule bank or the State Co operative Bank or such other co operative bank as the Government may notify in this behalf and the bank account shall be operated upon by such officers of the Board as may be authorised by the Board and in such manner as may be prescribed.

(2) The accounts of the Board as certified by the auditor, together with the audit report thereon shall be forwarded yearly to the Government and the Government may issue such instructions to the Board in respect thereof as they may deem fit and the Board shall comply with all such instructions. A copy of the accounts of the Boards certified by the Auditor together with a copy of the audit report thereon shall be laid on the table of the Legislative Assembly of the State.

**15\. Investment of Fund:-** Where the Fund or any portion thereof cannot be utilised within a period of ninety days for fulfilling the objects of the Act, the Board shall invest the same in any of the securities specified in clauses (a) to (d) of Section 20 of the Indian Trusts Act, 1882 or in fixed deposit bonds of the State Co operative Bank or such other Co operative Bank as the Government may notify from time to time in this behalf, or in such other security as may be expressly authorised by the Government in this behalf.

**16\. Directions by Government to Board:-** The Government may give to the Board such directions as in their opinion are necessary or expedient in connection with the expenditure from the Fund or for carrying out the purposes of this Act and it shall be the duty of the Board to comply with all such directions.

**17\. Appointment and powers of Welfare Commissioner:-** (1) The Board shall, with the previous approval of the Government, appoint an officer of the Labour Department not below the rank of Joint Commissioner of Labour, as Welfare Commissioner who shall be the Chief Executive Officer of the Board. It shall be the duty of the Welfare Commissioner to ensure that the provisions of this Act and the rules made thereunder are duly carried out, and for this purpose he shall have the power to issue such orders, not inconsistent with the provisions of this Act or the rules made thereunder, as he deems fit, including any order implementing the decisions of the Board taken by or under this Act.

(2) Notwithstanding anything in sub section (1), the first Welfare Commissioner shall be appointed by the Government as soon as may be precticable, after the commencement of this Act for such period, not exceeding five years, and on such conditions, as the Government may think fit.

**18\. Appointment of Inspectors :-** (1) The Government may appoint as many Inspectors as are required to inspect records in connection with the sums payable into the Fund.

(2) Any Inspector may

(a) with such assistance as he thinks fit, enter at any reasonable time any premises for carrying out the provisions of this Act;

(b) exercise such other powers as may be prescribed;

(c) make such examination and hold such enquiry as may be necessary for ascertaining whether the provisions of this Act, have been and are being complied with;

(d) require the production of any prescribed register and any other document in possession of the employer in connection with the sums payable to the Fund.

**19\. Appointment of clerical and other staff by Board:-** The Board shall have power to appoint such officers and clerical and executive staff as it thinks fit to carry out the purposes of the Fund and to supervise and control the activities of any other body financed from the Fund:

Provided that the expenditure on account of the officers and servants appointed by the Board and any other administrative expenses to be incurred by the Board shall not exceed fifteen percent of the annual income of the Fund.

**20\. Power of Government over staff of Board in certain cases:-** The Government shall have power to appoint any person to, or remove any such person from, the service of the Board in respect of whom the Board by a special resolution of more than one third of its members, so authorises the Government.

**21\. Allotment of certain officers and staff to the Board:-** (1) As soon as may be after the commencement of this Act, the Government may, after consulting the Board, direct by general or special order that such of the officers and other servants serving immediately before the notified date in connection with the affairs of the State as are specified in the order shall be allotted to serve in connection with the affairs of the Board with effect on and from such date as may be specified in the order;

Provided that no such direction shall be issued in respect of any officer or other servants without his consent for such allotment:

Provided further that the conditions of service applicable immediately before the notified date to any such person shall not be varied to his disadvantage except with the previous approval of the Government.

(2) With effect on and from the notified date, the officers and other servants specified in such order shall become employees of the Board and shall cease to be officer or servants of the Government.

_Explanation:-_ In this section, notified date means such date, as is notified by the Government in this behalf.

**22\. Transfer of Provident Fund to the Board:-** (1) The moneys standing to the credit under the Provident Fund Account of any officer or servant allotted from the service of the Government to the service of the Board on the date specified in the order under sub section (1) of Section 21 shall stand transferred to and vest in the Board with effect on and from such date.

(2) The Board shall, as soon as may be after such date, constitute in respect of the moneys transferred to and vested in it under sub section (1); a similar fund and may invest the accumulations under the fund in such securities and subject to such conditions, as may be specified by the Board with the approval of the Government.

**23\. Powers of Government or authorised officer to call for records, etc.:-** The Government or any officer authorised by them in this behalf, may call for and examine the records of the Board for the purpose of supervising the working of the Board and may pass such orders as they or he may think fit.

**24\. Mode of recovery of sums payable into the Fund, etc.:-** Any sum payable into the Fund under this Act shall, without prejudice to any other mode of recovery, be recoverable on behalf of the Board as an arrear of land revenue.

**25\. Penalty for obstructing inspection or for failure to produce documents, etc.:-** Any person who wilfully obstructs an Inspector in the exercise of his powers or discharge of his duties under this Act or fails to produce for inspection on demand by an Inspector any register, record or other document maintained in pursuance of the provisions of this Act or the rules made thereunder or to supply to him on demand true copies of any such document shall, on conviction, be punished,

(a) for the first offence with imprisonment for a term which may extend to three months, or with fine which may extend to five hundred rupees, or with both; and

(b) for a second or subsequent offence, with imprisonment for a term which may extend to six months or with fine which may extend to one thousand rupees, or with both;

Provided that in the absence of special and adequate reasons to the contrary to be mentioned in the judgment of the court in any case where the offender is sentenced to fine only, the amount of fine shall not be less than fifty rupees.

**26\. Cognizance of offences:-** (1) No court shall take cognizance of any offence punishable under this Act except on a complaint by, or with the previous sanction in writing of the Welfare Commissioner. (2) No court inferior to that of a Magistrate of first class shall try any offence punishable by or under this Act.

**27\. Offences by companies:-** (1) If the person committing an offence under this Act is a company, every person who, at the time the offence was committed, was incharge of, and was responsible to, the company for the conduct of the business of the company as well as the company shall be deemed to be guilty of the offence, and shall be liable to be proceeded against and punished accordingly:

Provided that nothing contained in this sub section shall render such person liable to any punishment provided in this Act, if he proves that the offence was committed without his knowledge, or that he exercised all due diligence to prevent the commission of the offence.

(2) Notwithstanding anything in sub section (1) where an offence under this Act has been committed by a company and it is proved that the offence has been committed with the consent or connivance of, or is attributable to any neglect on the part of any director, manager, secretary or other officer of the company, such director, manager, secretary or other officer shall also be deemed to be guilty of that offence and shall be liable to be proceeded against and punished accordingly.

_Explanation:-_ For the purposes of this section. company means a body corporate and includes a firm or other association of individuals; and director in relation to a firm means a partner in the firm

**28\. Limitation of prosecution:-** No court shall take cognizance of an offence punishable by or under this Act unless a complaint thereof is made within six months of the date on which the offence is alleged to have been committed.

**29\. Power to supervise the welfare activities of an establishment:-** In regard to any money set apart in any establishment specifically for the purpose of promoting the welfare of the employees in such establishment, the Board shall have power

(i) to require the production of any document in possession of the employer of the establishment in connection with such money to satisfy itself as to whether such money is being applied for such purposes;

(ii) to call for any such information from the employer of the establishment as it may deem relevant; and

(iii) to issue such directions to the employer of the establishment as it may deem fit for the purpose of utilising the Fund for promoting the welfare of the employees in the establishment.

**30\. Penalty for non compliance with the direction of the Board:-** Any person who wilfully fails to produce any document required by the Board or to furnish any information called for by the Board or to furnish any information called for by the Board or wilfully fails to comply with any direction issued by the Board under Section 29 shall, on conviction, be punished with

(a) for the first offence, with imprisonment for a term which may extend to three months, or with fine which may extend to five hundred rupees or with both; and

(b) for a second or subsequent offence, with imprisonment for a term which may extend to three months, or with fine which may extend to five hundred rupees or with both ; and

Provided that in the absence of special and adequate reasons to the contrary to be mentioned in the judgment of the court, in any case where the offender is sentenced to fine only, the amount of fine shall not be less than fifty rupees.

**31\. Annual Report:-** The Board shall as soon as may be after the end of each year, prepare and submit to the Government before such date and in such form as may be prescribed, a report giving an account of its activities during the previous year, and the report shall also give an account of the activities, if any, which are likely to be undertaken by the Board in the next year. A copy of such report shall be laid on the table of the Legislative Assembly of State.

**32\. Supersession of the Board:-** (1) If the Government are of opinion that the Board is unable to perform or has persistently made default in the performance of, the duty imposed on it by or under this Act or has exceeded or abused its powers, they may, by notification, supersede the Board for a period not exceeding six months as may be specified in the notification:

Provided that before issuing a notification under this sub section, the Government shall, by notice require the Board to show cause within such period as may be specified in the notice why it would not be superseded and shall consider the explanations and objections, if any, of the Board.

(2) Upon the publication of a notification under sub section (1) superseding the Board,

(a) the Chairman and all the members of the Board shall, as from the date of supersession, vacate their offices as such ;

(b) all the powers and duties which may, by or under the provisions of this Act be exercised or performed by or on behalf of the Board and the Chairman shall, during the period of supersession be exercised and performed by such authority or person as the Government may direct:

(c) all funds and other property vested in the Board shall, during the period of supersession, vest in the authority or person referred to in clause (b);and

(d) all liabilities, legally subsisting and enforceable against the Board shall be enforceable against the authority or person referred to in clause (b) to the extent the funds and properties vested in it or him.

(3) On the expiration of the period of supersession specified in the notification issued under Sub section (1), the Government may,

(a) extend the period of supersession for such further period not exceeding six months as these may consider necessary; or

(b) reconstitute the Board in the manner provided in sub section (3) of Section 4.

**33\. Delegation of powers:-** (1) The Government may, by notification; authorise any authority or officer to exercise any of the powers vested in them by or under this Act except the power to make rules under Section 37 and may in like manner withdraw such authorisation.

(2) The Board may by, general or special order in writing delegate to the Welfare Commissioner or other officer of the Board such of its powers and functions under this Act except the power to make regulations under Section 38, as it may deem necessary and it may in like manner withdraw such delegation.

(3) The exercise of any power delegted under sub section (1) or sub section (2) shall be subject to such restrictions and conditions as may be specified in the order and also to control and revision by the Government or by such officers as may be empowered by the Government in this behalf or, as the case may be by the Board or such officer as may be empowered by the Board in this behalf.

(4)The Government or the Board, as the case may be, shall also have the power to control and revise the acts and proceedings of any officer so empowered.

**34\. Members of Board, Welfare Commissioner, Inspectors and all officers and servants of the Board to be public servants :-** All the members of the Board. the Welfare Commissioner, Inspectors and all Officers and servants of the Board shall be deemed to be public servants within the meaning of section 21 of the Indian Penal Code, 1860.

**35\. Protection of action taken in good faith :-** (1) No suit, prosecution or other legal proceedings shall lie against any person for anything which is in good faith, done or intended to be done in pursuance of this Act or any rule or order made thereunder.

**36\. Exemption:-** The Government may, by notification, exempt any establishment or class of establishments from all or any of the provisions of this Act subject to such conditions as may be specified in the notification.

**37\. Power of Government to make rules:-** (1) The Government may, by notification, make rules to carry out all or any of the purposes of this Act.

(2) In particular and without prejudice to the generality of the foregoing power, such rule may provide for all or any of the following matters, namely:

(a) the agency for and the manner of collection of sums referred to in sub section (3) of Section 3, and the period within which the same shall be paid to the credit of the Fund;

(b) the manner in which the accounts of the Fund shall be maintained and audited under sub section (3) of Section 3;

(c) the procedure for making grants out of the Fund;

(d) the procedure for defraying the expenditure incurred in administrating the Fund;

(e) the number of representations of the employers and employees, on the Board;

(f) the allowances, if any, payable to them under Section 4;

(g) the procedure to be followed at the meetings of the Board and the manner in which the Board shall conduct its business;

(h) the particulars in the notice regarding unpaid accumulations;

(i) the duties and powers of the Welfare Commissioner and the Inspectors and the conditions of service of the Welfare Commissioner and the Inspectors and other staff appointed by the Board under this Act;

(j) the percentage of the annual income of the Fund beyond which the Board may not spend on the staff and on other administrative expenses;

(k) the registers and records to be maintained by the Board or its officers and servants under this Act including the register to be kept separately for the account of unpaid accumulations;

(l) the publication of the report of the activities of bodies financed from the Fund together with a statement of receipts and expenditure of the Fund and statement of accounts;

(m) any other matter which under this Act is or may be prescribed.

(3) Every rule made under this Act shall immediately after it is made, be laid before the Legislative Assembly of the State if it is in session and if it is not in session in the session immediately following for a total period of fourteen days which may be comprised in one session or in two successive sessions, and if, before the expiration of the session in which it is so laid or the session immediately following the Legislative Assembly agrees in making any modification in the rule or in the annulment of the rule, the rule shall, from the date on which the modification or annulment is notified, have effect only in such modified form or shall stand annulled as the case may be, so however that any modification or annulment shall be without prejudice to the validity of anything previously done under that rule.

**38\. Power of Board to make regulations:-** (1) The Board may, by notification, make regulations, not inconsistent with this Act and the rules made thereunder, for the purpose of giving effect to the provisions of this Act.

(2) In particular and without prejudice to the generality of the foregoing power, such regulations may provide for,

(a) all matters expressly required or allowed by this Act to be laid down by regulations;

(b) the terms and the conditions of appointment and service and the scales of pay of officers and servants of the Board, including the payment of travelling and daily allowances in respect of journeys undertaken by such officers and servants of the Board;

(c) the supervision and control over the acts and proceedings of the officers and servants of the Board and the maintenance of discipline and conduct among the officers and servants of the Board;

(d) the procedure in regard to the transaction of business at the meetings of the Board including the quorum:

(e) the purpose for which and the manner in which temporary association of persons may be made;

(f) the duties, the functions, the terms and conditions of service of the members of the committees;

(g) the manner and the form relating to the maintenance of the accounts of the Board.

(3) No regulation or its cancellation or modification shall have effect until the same shall have been approved by the Government.

(4) The Government may, by notification rescind any regulation made under this Section and thereupon, the regulation shall cease to have effect.

**39\. Amendment of Section 8 of the Central Act 4 of 1936:-** In sub section (8) of section 8 of the Payment of Wages Act, 1936, before the Explanation, the following shall be inserted, namely:

"but in the case of any establishment to which the Andhra Pradesh Labour Welfare Fund Act, 1987 applies, all such realisations shall be paid into the Fund constituted under the said Act".

**40\. Amendment of Section 8 of Central Act 8 of 1923:-** To sub section (4) of section 8 of the Workmen s Compensation Act, 1923, the following proviso shall be added, namely:

"Provided that in respect of a workman belonging to an establishment to which the Andhra Pradesh Labour Welfare Fund Act, 1987 applies, the Commissioner shall pay the said balance of the money into the Fund constituted under that Act in lieu of repaying it to the employer".
